import React, {Component} from 'react'

//Creating Table header as a simple component
const TableHeader = () => {
  return (
    <thead>
      <tr>
        <th>Name</th>
        <th>Job Title</th>
      </tr>
    </thead>
  )
}

const TableBody = (props) => {
  const rows = props.characterData.map((info, index) => {
    return (
      <tr key={index}>
        <td>{info.name}</td>
        <td>{info.job}</td>
        <td>
          <button onClick={() => props.removeCharacter(index)}>Delete</button>
        </td>
      </tr>
    )
  });
  return (
    <tbody>
      {rows}
    </tbody>
  )
}

const Table = (props) => {
  const {characterData, removeCharacter} = props;
  return (
    <table>
      <TableHeader />
      <TableBody characterData={characterData} removeCharacter={removeCharacter} />
    </table>
  )
}

export default Table
