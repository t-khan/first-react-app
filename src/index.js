import React, {Component} from 'react'
import ReactDOM from 'react-dom';
import './index.css';

import App from "./App" //You may or may not put .js 



ReactDOM.render(
  <App />,
  document.getElementById('root')
);
